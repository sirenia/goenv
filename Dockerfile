FROM golang:1.24-alpine

ENV GOPATH=/go

# misc tooling needed
RUN apk --update add git openssl unzip curl 
RUN apk add upx || true

# go tooling
RUN go install github.com/mna/pigeon@latest
RUN go install github.com/go-swagger/go-swagger/cmd/swagger@latest
RUN go install github.com/gobuffalo/packr/v2/packr2@latest
RUN go install github.com/mitchellh/gox@latest
RUN go install github.com/golang/mock/mockgen@latest
RUN go install github.com/sonatype-nexus-community/nancy@latest
RUN go install github.com/goreleaser/goreleaser@latest
RUN go install github.com/jstemmer/go-junit-report/v2@latest
RUN go install golang.org/x/tools/cmd/goimports@latest
RUN go install golang.org/x/vuln/cmd/govulncheck@latest
RUN go install github.com/google/osv-scanner/cmd/osv-scanner@latest
RUN go install golang.org/x/tools/cmd/deadcode@latest

ADD minimock /usr/local/bin

# gRPC tooling
RUN go install google.golang.org/protobuf/cmd/protoc-gen-go@latest
RUN go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest
RUN go install github.com/golang/protobuf/protoc-gen-go@latest 

# Doc gen for grpc
RUN go install github.com/pseudomuto/protoc-gen-doc/cmd/protoc-gen-doc@latest


# Install golangci
RUN wget -O- -nv https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s v1.60.1

# protobuf
ENV PROTOBUF_VERSION=27.3

ARG TARGETARCH

RUN if [ "$TARGETARCH" = "arm64" ]; then \
       PROTOC_ZIP=protoc-${PROTOBUF_VERSION}-linux-aarch_64.zip; \ 
    else \
       PROTOC_ZIP=protoc-${PROTOBUF_VERSION}-linux-x86_64.zip; \ 
    fi && \
    curl -OL https://github.com/protocolbuffers/protobuf/releases/download/v${PROTOBUF_VERSION}/${PROTOC_ZIP} && \
    unzip -o ${PROTOC_ZIP} -d ./proto && \
    chmod 755 -R ./proto/bin && \
    cp ./proto/bin/protoc /usr/bin/ && \
    cp -R ./proto/include/* /usr/include/
    

WORKDIR $GOPATH

# Cleanup.
RUN apk del unzip openssl
#RUN find /usr/lib -name "*.a" -delete -or -name "*.la" -delete 
RUN apk add --no-cache libstdc++ musl-dev coreutils gcc g++ libprotobuf libprotoc protobuf-dev protoc

# Add swagger-ui
ADD swagger-ui-5.11.7 /swagger-ui
